/*给用户主动调用的 您不需要改动 或者重写
* 只要 var api = new MagicalApi();
* 即可使用api.getHtml();等各种方法
* */
function MagicalApi() {

}

/**
 * 获取布局器HTML源码
 */
MagicalApi.prototype.getHtml=function () {}
/**
 * 获取布局器脚本
 */
MagicalApi.prototype.getJavascript=function () {}
/**
 * 获取布局器根节点json
 */
MagicalApi.prototype.getRootNode=function () {}
/**
 * 能获取表单条目下的控件数据
 */
MagicalApi.prototype.getFormItemNodes = function (){}
/**
 * 插入源码到布局器
 * @param html
 */
MagicalApi.prototype.insertHtml=function (html) {}
/**
 * 插入javascript脚本 请尽量在insertHtml方法之后执行 因为大部分脚本操作的是html 必须先有html
 * @param javascript
 */
MagicalApi.prototype.insertJavascript=function (javascript) {}
/**
 * 插入html javascript 常用于初始化布局器
 * @param html
 * @param javascript
 */
MagicalApi.prototype.insert=function (html,javascript) {}

/*根据root 还原布局器*/
MagicalApi.prototype.insertNode=function (rootNode) {}
